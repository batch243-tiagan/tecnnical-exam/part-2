let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    let manualLength = 0;
     while(collection[manualLength] != undefined){
        manualLength += 1;
     }
     collection[manualLength] = element; 

     // return i == 0;

    // let index = 0;
    // for(i = 0; i < collection.length; i++){
    //     index += 1;
    // }
    // collection[index] = element;

    return collection;
}

function dequeue() {
    // In here you are going to remove the first element in the array
    let manualLength = 0;
     while(collection[manualLength] != undefined){
        manualLength += 1;
     }


    let tempArr = [];
    for(i = 0; i < manualLength; i++){
        tempArr[i] = collection[i+1];
        tempArr.length = manualLength -1;
    }
    collection = tempArr;

    return collection;
}

function front() {
    let manualLength = 0;
     while(collection[manualLength] != undefined){
        manualLength += 1;
    }

   let i = manualLength - (manualLength - 1);
   return collection[i - 1];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
    let i = 0;
     while(collection[i] != undefined){
        i += 1;
     } 
     return i;
}

function isEmpty() {
    let i = 0;
     while(collection[i] != undefined){
        i += 1;
     } 
     return i == 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};